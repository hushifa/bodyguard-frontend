import { createApp } from "vue";
import Antd from "ant-design-vue";
import "@/assets/css/reset.css";
import "ant-design-vue/dist/antd.css";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { globalAxios } from "./request";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from "@element-plus/icons-vue";

const app = createApp(App);

// 使用antd
app.use(Antd);

// 使用全局axios
app.use(globalAxios);

// 使用vuex
app.use(store);

// 使用路由
app.use(router);

// 使用ElementPlus
app.use(ElementPlus);
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

router.isReady().then(() => app.mount("#app"));
