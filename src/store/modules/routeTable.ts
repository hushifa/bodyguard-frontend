export default [
  {
    path: '/',
    redirect: '/user',
    hidden: true,
  },
  {
    id: 101,
    name: "用户",
    path: "/user",
    redirect: "/user/member",
    component: "BasicLayout",
    key: "user",
    children: [
      {
        id: 1011,
        name: "会员管理",
        path: "/user/member",
        redirect: "/user/member/compony",
        component: "RouteView",
        key: "member",
        children: [
          {
            id: 10111,
            name: "企业会员",
            path: "/user/member/compony",
            component: "/user/member/compony",
            key: "compony"
          },
          {
            id: 10112,
            name: "个人会员",
            path: "/user/member/personal",
            component: "/user/member/personal",
            key: "personal"
          }
        ]
      },
      {
        id: 1012,
        name: "个人简历管理",
        path: "/user/urm",
        redirect: "/user/urm/urmlist",
        component: "RouteView",
        key: "urm",
        children: [
          {
            id: 10121,
            pid: 1012,
            name: "全部简历",
            path: "/user/urm/urmlist",
            component: "/user/urm/urmlist",
            key: "urmlist"
          },
          {
            id: 10123,
            pid: 1014,
            name: "增加简历",
            path: "/user/urm/urmadd",
            component: "/user/urm/urmAdd",
            key: "urmadd",
            hidden: true
          }
        ]
      },
      {
        id: 1013,
        name: "企业信息管理",
        path: "/user/compony",
        redirect: "/user/compony/whole",
        component: "RouteView",
        key: "crm",
        children: [
          {
            id: 10131,
            name: "全部线索",
            path: "/user/compony/whole",
            component: "/user/compony/whole",
            key: "whole"
          },
          {
            id: 10136,
            name: "新增线索",
            path: "/user/compony/wholeAdd",
            component: "/user/compony/wholeAdd",
            key: "wholeAdd",
            hidden: true
          },
          {
            id: 10137,
            name: "新增客户",
            path: "/user/compony/allclientAdd",
            component: "/user/compony/allclientAdd",
            key: "allclientAdd",
            hidden: true
          },
          {
            id: 10132,
            name: "我的线索",
            path: "/user/compony/my",
            component: "/user/compony/my",
            key: "my"
          },
          {
            id: 10133,
            name: "线索公海",
            path: "/user/compony/internationalwaters",
            component: "/user/compony/internationalwaters",
            key: "internationalwaters"
          },
          {
            id: 10134,
            name: "全部客户",
            path: "/user/compony/allclient",
            component: "/user/compony/allclient",
            key: "allclient",
          },
          {
            id: 10135,
            name: "我的客户",
            path: "/user/compony/myclient",
            component: "/user/compony/myclient",
            key: "myclient"
          },
          {
            id: 10135,
            name: "客户公海",
            path: "/user/compony/pubilceclient",
            component: "/user/compony/pubilceclient",
            key: "pubilceclient"
          }
        ]
      },
      {
        id: 1013,
        name: "职位信息管理",
        path: "/user/job",
        redirect: "/user/job/list",
        component: "RouteView",
        key: "job",
        children: [
          {
            id: 10131,
            name: "职位管理",
            path: "/user/job/list",
            component: "/user/job/list",
            key: "list"
          },
          {
            id: 10132,
            name: "编辑职位",
            path: "/user/job/putlist",
            component: "/user/job/putlist",
            key: "putlist",
            hidden: true
          }
        ]
      },
    ]
  },
  {
    id: 102,
    name: "内容",
    path: "/content",
    redirect: "/content/ad",
    component: "BasicLayout",
    key: "content",
    children: [
      {
        id: 1021,
        name: "广告管理",
        path: "/content/ad",
        redirect: "/content/ad/list",
        component: "RouteView",
        key: "ad",
        children: [
          {
            id: 10211,
            name: "广告列表",
            path: "/content/ad/list",
            component: "/contents/ad/list",
            key: "ad_list"
          },
          {
            id: 10212,
            name: "广告位",
            path: "/content/ad/category",
            component: "/contents/ad/category",
            key: "ad_category"
          },
          {
            id: 10213,
            name: "编辑广告位",
            path: "/content/ad/category_edit",
            component: "/contents/ad/category_edit",
            key: "ad_category_edit",
            hidden: true
          },
          {
            id: 10214,
            name: "添加广告位",
            path: "/content/ad/category_add",
            component: "/contents/ad/category_add",
            key: "ad_category_add",
            hidden: true
          },
          {
            id: 10215,
            name: "添加广告",
            path: "/content/ad/list_add",
            component: "/contents/ad/list_add",
            key: "ad_list_add",
            hidden: true
          },
          {
            id: 10216,
            name: "编辑广告",
            path: "/content/ad/list_edit",
            component: "/contents/ad/list_edit",
            key: "ad_list_edit",
            hidden: true
          },
        ]
      },

    ]
  },
  {
    id: 103,
    name: "系统",
    path: "/sys",
    redirect: "/sys/category",
    component: "BasicLayout",
    key: "sys",
    children: [
      {
        id: 1031,
        name: "分类配置",
        path: "/sys/category",
        redirect: "/sys/category/jobcategory",
        component: "RouteView",
        key: "sys_category",
        children: [
          // {
          //   id: 10311,
          //   name: "地区分类",
          //   path: "/sys/category/district",
          //   component: "/sys/category/district",
          //   key: "category_district"
          // },
          {
            id: 10312,
            name: "职位分类",
            path: "/sys/category/jobcategory",
            component: "/sys/category/jobcategory",
            key: "category_jobcategory"
          },
          {
            id: 10313,
            name: "专业分类",
            path: "/sys/category/major",
            component: "/sys/category/major",
            key: "category_major"
          }
        ]
      },
      {
        id: 1032,
        name: "系统管理员",
        path: "/sys/admin",
        redirect: "/sys/admin/list",
        component: "RouteView",
        key: "sys_admin",
        children: [
          {
            id: 10321,
            name: "管理员列表",
            path: "/sys/admin/list",
            component: "/sys/admin/list",
            key: "admin_list"
          },
          {
            id: 10322,
            name: "添加管理员",
            path: "/sys/admin/listAdd",
            component: "/sys/admin/listAdd",
            key: "admin_listAdd",
            hidden: true
          },
          {
            id: 10323,
            name: "编辑管理员",
            path: "/sys/admin/listUpdate",
            component: "/sys/admin/listUpdate",
            key: "admin_listUpdate",
            hidden: true
          },
          {
            id: 10324,
            name: "角色权限管理",
            path: "/sys/admin/role",
            component: "/sys/admin/role",
            key: "admin_role"
          },
          {
            id: 10325,
            name: "添加角色",
            path: "/sys/admin/roleAdd",
            component: "/sys/admin/roleAdd",
            key: "admin_roleAdd",
            hidden: true
          },
          {
            id: 10326,
            name: "编辑角色",
            path: "/sys/admin/roleUpdate",
            component: "/sys/admin/roleUpdate",
            key: "admin_roleUpdate",
            hidden: true
          }
        ]
      }
    ]
  }
]