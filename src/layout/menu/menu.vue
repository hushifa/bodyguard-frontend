<template>
  <div class="menu__logo">
    <span>后台管理中心</span>
  </div>
  <a-menu theme="dark" mode="inline" v-model:selectedKeys="selectedKeys" v-model:openKeys="openKeys">
    <create :router="item" v-for="item in menuRouter" :key="item.id" />
  </a-menu>
</template>
<script lang="ts">
import create from './menu-create'
import { useRoute } from 'vue-router'
import { mapState, useStore } from 'vuex'
import aIcon from '@/components/aicon/aicon.vue'
import { defineComponent, ref, watch, onBeforeMount } from "vue"
import storage from 'store'
export default defineComponent({
  name: 'layoutMenu',
  computed: {
    ...mapState({
      menuRouter: (state: any) => state.menu.menuRouter
    })
  },
  components: {
    create,
    aIcon
  },
  props: {
    collapsed: {
      required: true,
      type: Boolean
    }
  },
  setup() {

    const store = useStore()
    const route = useRoute()
    const selectedKeys = ref<string[]>([])
    const openKeys = ref<string[]>([])

    const setMenuKey = () => {
      if (!route.meta.hidden) {
        selectedKeys.value = [route.name as string]
        openKeys.value = []
        route.matched.forEach(item => {
          openKeys.value.push(item.name as string)
        })
        storage.set('openKeys', openKeys.value)
        storage.set('selectedKeys', selectedKeys.value)
        // 设置顶部tab(栏目)切换
        store.commit('menu/setId', route.matched[0]['meta']['id'])
      } else {
        // 解决隐藏的路由,刷新后左侧菜单丢失的问题
        selectedKeys.value = storage.get('selectedKeys')
        openKeys.value = storage.get('openKeys')
        store.commit('menu/setId', route.matched[0]['meta']['id'])
      }
    }

    onBeforeMount(setMenuKey)
    watch(route, setMenuKey)

    return { selectedKeys, openKeys }

  }
})
</script>
<style lang="scss" scoped>
.menu__logo {
  height: 60px;
  line-height: 60px;
  overflow: hidden;
  white-space: nowrap;
  background-color: #2b2f3a;

  & span {
    display: inline-block;
    font-size: 20px;
    color: #fff;
    padding-left: 22px;
    cursor: pointer;
    user-select: none;
  }
}
</style>
<style lang="scss">
.ant-menu .ant-menu-submenu-title {
  margin: 0;
  height: 50px !important;

  i {

    &::after,
    &::before {
      background: rgb(191, 203, 217) !important;
    }
  }

  &:hover {
    background-color: #263445 !important;
  }
}

.ant-layout-sider-children,
.ant-menu {
  background-color: #304156 !important;


  span {
    color: rgb(191, 203, 217) !important;
  }

  .ant-menu-item,
  .ant-menu-sub {
    li {
      margin: 0 !important;
      height: 50px !important;

      &:hover {
        background-color: #001528 !important;
      }
    }

    a {
      color: rgb(191, 203, 217) !important;
    }


    background-color: #1f2d3d !important;
  }



  .ant-menu-item-selected a {
    color: rgb(64, 158, 255) !important;
  }
}
</style>