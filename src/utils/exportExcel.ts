import XLSX from "xlsx";
/*
    * @description:
    * @param {Object} json 服务端发过来的数据
    * @param {String} name 导出Excel文件名字
    * @param titleObj 一个存放标题的字段（请根据展示顺序编写写） 格式如下  
        titleObj={
            中文名称：字段名
        }
    * @param {String} sheetName 导出sheetName名字
*/
export function exportExcel(json: any, name: string, titleObj: any, sheetName: string) {
    // 获取 所需要的中英文字段名
    let filterTitle: any = getObjVal(titleObj, "value"); // 英文字段
    let titleArr: any = getObjVal(titleObj, "key"); // 中文字段

    let data = []; // 最终存放的数据
    let tempData: any[] = []; // 筛选出的乱序数据
    let sortData = [] as any; // 排序后的数据
    // 筛选出符合条件的服务端数据
    for (const key1 in json) {
        if (json.hasOwnProperty(key1)) {
            const element = json[key1];
            let rowArr = [];
            for (const key2 in element) {
                if (element.hasOwnProperty(key2) && filterTitle.includes(key2)) {
                    rowArr.push({
                        label: key2,
                        value: element[key2]
                    });
                }
            }
            tempData.push(rowArr);
        }
    }
    // 对符合条件数据 按照titleObj 进行排序
    tempData.map((item: any) => {
        let arr = [] as any;
        filterTitle.map((item1: any) => {
            item.map((item2: any) => {
                if (item1 === item2.label) {
                    arr.push(item2.value);
                }
            });
        });
        sortData.push(arr);
    });

    // //  隐藏英文字段 数据组装
    // data = [filterTitle, titleArr, ...sortData];
    // console.log("data", data);
    // const ws = XLSX.utils.aoa_to_sheet(data);
    // const wb = XLSX.utils.book_new();
    // 此处隐藏英文字段表头
    // let wsrows = [{ hidden: true }];
    // ws["!rows"] = wsrows; // ws - worksheet
    // XLSX.utils.book_append_sheet(wb, ws, sheetName);
    /* generate file and send to client */
    // XLSX.writeFile(wb, name + ".xlsx");


    // 不要英文字段
    // 数据组装
    data = [titleArr, ...sortData];
    console.log("data", data);
    const ws = XLSX.utils.aoa_to_sheet(data);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, sheetName);
    XLSX.writeFile(wb, name + ".xlsx");
}

/*
    * @description: 分别获取对象的键 或 值
    * @param {titleObj}  原始对象
    * @flag 标记  key 取键  value 取值 
*/
export function getObjVal(titleObj: any, flag: string) {
    let arr = [];
    // key 取key值
    if (flag == "key") {
        for (const key in titleObj) {
            if (titleObj.hasOwnProperty(key)) {
                arr.push(key);
            }
        }
        return arr;
    }
    // value 取value 值
    if (flag == "value") {
        for (const key in titleObj) {
            if (titleObj.hasOwnProperty(key)) {
                arr.push(titleObj[key]);
            }
        }
        return arr;
    }
}